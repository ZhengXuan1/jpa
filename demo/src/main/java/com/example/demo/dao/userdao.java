package com.example.demo.dao;

import com.example.demo.module.User;
import org.springframework.data.repository.CrudRepository;

public interface userdao extends CrudRepository<User,Integer> {

}
