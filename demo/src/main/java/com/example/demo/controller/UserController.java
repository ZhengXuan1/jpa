package com.example.demo.controller;

import com.example.demo.dao.userdao;
import com.example.demo.module.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    @Autowired
    userdao dao;

    @RequestMapping("/")
    public String html(){
        return "login";
    }

    @RequestMapping("/adduser")
    public String adduser(User user){
        dao.save(user);
        return "login";
    }
}
